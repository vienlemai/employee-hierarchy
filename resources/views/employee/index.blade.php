@extends('layouts.master')
@section('content')
<div class="container">
    <h3>Employee Hierarchy</h3>
    <form action="{{ route('employee.graph') }}" method="post" enctype="multipart/form-data" id="form-upload-employee">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="employee-file">Upload employee json file</label>
            <input type="file" name="file" class="form-control" id="employee-file" placeholder="Employee json file">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <div id="tree"></div>
</div>
@endsection