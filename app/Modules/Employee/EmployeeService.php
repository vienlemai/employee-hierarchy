<?php
namespace App\Modules\Employee;

use Illuminate\Support\Collection;
use InvalidArgumentException;

class EmployeeService
{
    /**
     * Find the root supervisor
     *
     * @return array
     */
    public function findRootSuppervisor(Collection $employees) : string
    {
        $onlyEmployees = $employees->pluck('employee_name');
        $roots = $employees->filter(function ($item) use ($onlyEmployees) {
            return !$onlyEmployees->contains($item['supervisor_name']);
        })->values();
        if ($roots->isEmpty()) {
            throw new InvalidArgumentException('There is a loop in employee hierarchy');
        }
        if ($roots->count() > 1) {
            throw new InvalidArgumentException('There are more than 1 bosses: ' . $roots->pluck('supervisor_name')->implode(', '));
        }
        return $roots->first()['supervisor_name'];
    }

    /**
     * Check if an employee has subbordinates
     *
     * @param Collection $employees
     * @param string $employeeName
     * @return boolean
     */
    public function hasSubbordinates(Collection $employees, string $employeeName) : bool
    {
        $subbordinates = $employees->filter(function ($item) use ($employeeName) {
            return $item['supervisor_name'] == $employeeName;
        })->count();
        
        return $subbordinates > 0;
    }
}
