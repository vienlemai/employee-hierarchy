<?php
namespace App\Modules\Employee\Formatter;

use App\Modules\Employee\Formatter\Contracts\FormatterInterface;
use App\Modules\Employee\Models\EmployeeNode;

class NormalFormatter implements FormatterInterface
{
    /**
     * Format employee to normal format
     * Example: [ 'A' => [['B' => [['C' => []]]]]]
     *
     * @param EmployeeNode $employeeNode
     * @return array
     */
    public function format(EmployeeNode $employeeNode) : array
    {
        $node[$employeeNode->getEmployeeName()] = [];
        $subbordinates = $employeeNode->getSubbordinates();
        if (!empty($subbordinates)) {
            foreach ($subbordinates as $subbordinate) {
                $node[$employeeNode->getEmployeeName()][] = $this->format($subbordinate);
            }
        }
        return $node;
    }
}
