<?php
namespace App\Modules\Employee\Formatter\Contracts;

use App\Modules\Employee\Models\EmployeeNode;

interface FormatterInterface
{
    public function format(EmployeeNode $employeeNode) : array ;
}
