<?php
namespace App\Modules\Employee\Formatter;

use App\Modules\Employee\Formatter\Contracts\FormatterInterface;
use App\Modules\Employee\Models\EmployeeNode;

class GraphFormatter implements FormatterInterface
{
    /**
     * Format employee to a format that can display on webview
     * Example: [ 'text' => A', 'children' => [ 'text' => 'B', 'children' => [ 'text' => 'C', 'children' => []]]]
     *
     * @param EmployeeNode $employeeNode
     * @return array
     */
    public function format(EmployeeNode $employeeNode) : array
    {
        $node['text'] = $employeeNode->getEmployeeName();
        $node['children'] = [];
        $subbordinates = $employeeNode->getSubbordinates();
        if (!empty($subbordinates)) {
            foreach ($subbordinates as $subbordinate) {
                $node['children'][] = $this->format($subbordinate);
            }
        }
        return $node;
    }
}
