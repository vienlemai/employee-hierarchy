<?php
namespace App\Modules\Employee;

use Illuminate\Http\UploadedFile;
use Storage;

class EmployeeParser
{
    /**
     * Parse employee array from request to a new format that Laravel can validate
     * Input: [ 'Employee1' => 'Supervisor1', ... ]
     * After parse: [['employee_name' => 'Employee1', 'supervisor_name' => 'Supervisor1'], [...]]
     *
     * @param array $employees
     * @return array
     */
    public function parse(array $employees) : array
    {
        return collect($employees)->map(function ($supervisorName, $employeeName) {
            return [
                'employee_name' => trim($employeeName),
                'supervisor_name' => trim($supervisorName)
            ];
        })->values()
        ->toArray();
    }

    /**
     * Parse from uploaded file
     *
     * @param UploadedFile $uploadedFile
     * @return array
     */
    public function parseFromUploadedFile(UploadedFile $uploadedFile) : array
    {
        $path = $uploadedFile->store('json');
        $fileContents = Storage::get($path);
        $employees = json_decode($fileContents, true);
        return $this->parse($employees);
    }
}