<?php
namespace App\Modules\Employee;

use App\Modules\Employee\Models\EmployeeNode;
use Illuminate\Support\Collection;
use App\Modules\Employee\EmployeeService;

class TreeBuilder
{
    /**
     * EmployeeService
     *
     * @var EmployeeService
     */
    protected $employeeService;

    /**
     * Constructor
     *
     * @param EmployeeService $employeeService
     */
    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    /**
     * Recusive function to build EmployeeService tree
     *
     * @param Collection $employees
     * @param array $root
     * @return EmployeeNode
     */
    protected function buildTreeRecursive(Collection $employees, string $bossName) : EmployeeNode
    {
        $employeeNode = new EmployeeNode($bossName);
        foreach ($employees as $employee) {
            if ($employee['supervisor_name'] == $bossName) {
                $hasSubbordinate = $this->employeeService->hasSubbordinates($employees, $employee['employee_name']);
                if ($hasSubbordinate) {
                    $subbordinate = $this->buildTreeRecursive($employees, $employee['employee_name']);
                    $employeeNode->addSubbordinate($subbordinate);
                } else {
                    $employeeNode->addSubbordinate(new EmployeeNode($employee['employee_name']));
                }
            }
        }
        return $employeeNode;
    }

    /**
     * Find the root and build hierarchy tree
     *
     * @param Collection $employees
     * @return EmployeeNode
     */
    public function buildTree(Collection $employees) : EmployeeNode
    {
        $rootSupervisor = $this->employeeService->findRootSuppervisor($employees);
        $employeeNode = $this->buildTreeRecursive($employees, $rootSupervisor);
        return $employeeNode;
    }
}
