<?php
namespace App\Modules\Employee\Models;

use App\Modules\Employee\Formatter\Contracts\FormatterInterface;

class EmployeeNode
{
    /**
     * Employee name
     *
     * @var string
     */
    protected $employeeName;

    /**
     * The list children of current employee
     *
     * @var array
     */
    protected $subbordinates = [];

    /**
     * Constructor
     *
     * @param string $employeeName
     */
    public function __construct(string $employeeName)
    {
        $this->employeeName = $employeeName;
    }

    /**
     * Get employee name
     *
     * @return string
     */
    public function getEmployeeName() : string
    {
        return $this->employeeName;
    }

    /**
     * Get children of current employee
     *
     * @return array
     */
    public function getSubbordinates() : array
    {
        return $this->subbordinates;
    }

    /**
     * Add a child to current employee
     *
     * @param EmployeeNode $child
     * @return EmployeeNode
     */
    public function addSubbordinate(EmployeeNode $subbordinate) : EmployeeNode
    {
        $this->subbordinates[] = $subbordinate;
        return $this;
    }

    /**
     * Format the employee node by formater
     *
     * @param FormaterInterface $formater
     * @return array
     */
    public function format(FormatterInterface $formatter): array
    {
        return $formatter->format($this);
    }
}
