<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\GraphEmployeeRequest;
use App\Modules\Employee\Formatter\NormalFormatter;
use App\Modules\Employee\TreeBuilder;
use App\Modules\Employee\Formatter\GraphFormatter;

class EmployeeController extends Controller
{
    public function employeeApi(EmployeeRequest $request)
    {
        $employees = collect($request->input('employees'));
        $tree = app(TreeBuilder::class)
            ->buildTree($employees)
            ->format(new NormalFormatter);
        return response()->json($tree);
    }

    public function index()
    {
        return view('employee.index');
    }

    public function graph(GraphEmployeeRequest $request)
    {
        $employees = collect($request->input('employees'));
        $tree = app(TreeBuilder::class)
            ->buildTree($employees)
            ->format(new GraphFormatter);
        return response()->json($tree);
    }
}
