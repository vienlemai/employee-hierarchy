<?php

namespace App\Http\Requests;

use Storage;
use App\Modules\Employee\EmployeeParser;

class GraphEmployeeRequest extends EmployeeRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'file' => 'required',
        ];
        return array_merge(parent::rules(), $rules);
    }

    /**
     * Parse data from file to new format that Laravel can validate
     *
     * @return void
     */
    protected function parseEmployee()
    {
        $input = $this->request->all();
        $input['employees'] = app(EmployeeParser::class)->parseFromUploadedFile($this->file('file'));
        $this->replace($input);
    }
}
