<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use App\Modules\Employee\EmployeeParser;
use App\Rules\UnduplicatedEmployee;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employees' => 'required',
            'employees.*.supervisor_name' => 'required',
            'employees.*.employee_name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'employees.required' => 'Invalid json format',
        ];
    }

    /**
     * Override
     *
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->parseEmployee();
    }

    /**
     * Parse employee to a new format that Laravel can validate
     *
     * @return void
     */
    protected function parseEmployee()
    {
        $employees = app(EmployeeParser::class)->parse($this->all());
        $employeesInput['employees'] = $employees;
        $this->replace($employeesInput);
    }
}
