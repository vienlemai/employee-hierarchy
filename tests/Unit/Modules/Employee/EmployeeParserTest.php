<?php

namespace Tests\Unit\Modules\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Modules\Employee\EmployeeParser;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class EmployeeParserTest extends TestCase
{
    private $employeeParser;

    public function setUp()
    {
        $this->employeeParser = new EmployeeParser;
        parent::setUp();
    }

    public function testItCanParseEmployee()
    {
        $data = [ 'Pete'=> 'Nick', 'Barbara'=> 'Nick' ];
        $expectedDataParsed = [
            [ 'employee_name' => 'Pete', 'supervisor_name' => 'Nick' ],
            [ 'employee_name' => 'Barbara', 'supervisor_name' => 'Nick' ],
        ];
        $employees = $this->employeeParser->parse($data);
        $this->assertSame($employees, $expectedDataParsed);
    }
}
