<?php

namespace Tests\Unit\Modules\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Modules\Employee\EmployeeParser;
use App\Modules\Employee\EmployeeService;

class EmployeeServiceTest extends TestCase
{
    /**
     * @var EmployeeService
     */
    private $employeeService;

    /**
     * @var EmployeeParser
     */
    private $employeeParser;

    public function setUp()
    {
        $this->employeeService = new EmployeeService;
        $this->employeeParser = new EmployeeParser;
    }

    public function testItCanFindRootSupervisor()
    {
        $data = [ 'Pete'=> 'Nick', 'Barbara'=> 'Nick', 'Nick'=> 'Sophie', 'Sophie'=> 'Jonas' ];
        $employees = collect($this->employeeParser->parse($data));
        $root = $this->employeeService->findRootSuppervisor($employees);
        $this->assertSame('Jonas', $root);
    }

    /**
     * @dataProvider invalidEmployeesDataProvider
     */
    public function testItCantFindRootWithInvalidData($data, $exception, $message)
    {
        $employees = collect($this->employeeParser->parse($data));
        $this->expectException($exception);
        $this->expectExceptionMessage($message);
        $this->employeeService->findRootSuppervisor($employees);
    }

    public function invalidEmployeesDataProvider()
    {
        return [
            //2 bosses
            [
                [ 'Pete'=> 'Nick', 'Nick'=> 'Sophie','Marry'=> 'Jonas' ],
                \InvalidArgumentException::class,
                'There are more than 1 bosses: Sophie, Jonas'
            ],
            //Loop
            [
                [ 'Pete'=> 'Nick', 'Nick'=> 'Sophie', 'Sophie'=> 'Pete' ],
                \InvalidArgumentException::class,
                'There is a loop in employee hierarchy'
            ]
        ];
    }

    /**
     * @dataProvider findFirstSubbordinateDataProvider
     */
    public function testHasSubbordinates($data, $employeeName, $correctSubbordinate)
    {
        $employees = collect($this->employeeParser->parse($data));
        $subbordinate = $this->employeeService->hasSubbordinates($employees, $employeeName);
        $this->assertSame($correctSubbordinate, $subbordinate);
    }

    
    public function findFirstSubbordinateDataProvider()
    {
        return [
            //2 subbordinate
            [
                [ 'Pete'=> 'Nick', 'Barbara'=> 'Nick', 'Nick'=> 'Sophie', 'Sophie'=> 'Jonas' ],
                'Nick',
                true
            ],

            //1 subbordinate
            [
                [ 'Pete'=> 'Nick', 'Nick'=> 'Sophie', 'Sophie'=> 'Jonas' ],
                'Sophie',
                true
            ],

            //No subbordinate
            [
                [ 'Pete'=> 'Nick', 'Barbara'=> 'Nick', 'Nick'=> 'Sophie', 'Sophie'=> 'Jonas' ],
                'Pete',
                false
            ]
        ];
    }
}
