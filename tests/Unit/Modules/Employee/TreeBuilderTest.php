<?php

namespace Tests\Unit\Modules\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Modules\Employee\TreeBuilder;

class TreeBuilderTest extends TestCase
{
    public function testItCanBuildATree()
    {
        $input = collect([
            ['employee_name'=>'Pete','supervisor_name'=>'Nick'],
            ['employee_name'=>'Barbara','supervisor_name'=>'Nick'],
            ['employee_name'=>'Nick','supervisor_name'=>'Sophie'],
            ['employee_name'=>'Sophie','supervisor_name'=>'Jonas']
        ]);
        $tree = app(TreeBuilder::class)->buildTree($input);
        $this->assertEquals($tree->getEmployeeName(), 'Jonas');
        $this->assertEquals($tree->getSubbordinates()[0]->getEmployeeName(), 'Sophie');
        $this->assertEquals($tree->getSubbordinates()[0]->getSubbordinates()[0]->getEmployeeName(), 'Nick');
        $subbordinatesOfNick = $tree->getSubbordinates()[0]->getSubbordinates()[0]->getSubbordinates();
        $this->assertEquals($subbordinatesOfNick[0]->getEmployeeName(), 'Pete');
        $this->assertEquals($subbordinatesOfNick[0]->getSubbordinates(), []);
        $this->assertEquals($subbordinatesOfNick[1]->getEmployeeName(), 'Barbara');
        $this->assertEquals($subbordinatesOfNick[1]->getSubbordinates(), []);
    }
}
