<?php

namespace Tests\Unit\Modules\Employee\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Modules\Employee\Models\EmployeeNode;
use App\Modules\Employee\Formatter\Contracts\FormatterInterface;

class EmployeeNodeTest extends TestCase
{
    public function testItCanGetEmployeeName()
    {
        $employeNode = new EmployeeNode('Nick');
        $employeName = $employeNode->getEmployeeName();
        $this->assertEquals($employeName, 'Nick');
    }

    /**
     * @dataProvider subbordinatesProvider
     */
    public function testItCanGetSubbordinates(EmployeeNode $employeeNode, array $expectedSubbordinates)
    {
        $subbordinates = $employeeNode->getSubbordinates();
        $this->assertEquals($subbordinates, $expectedSubbordinates);
    }

    public function subbordinatesProvider()
    {
        return [
            [
                new EmployeeNode('Nick'),
                []
            ],
            [
                (new EmployeeNode('Nick'))->addSubbordinate(new EmployeeNode('Pete')),
                [ new EmployeeNode('Pete') ]
            ]
        ];
    }

    public function testItCanAddASubbordinateWhenSubbordinatesAreEmpty()
    {
        $employeeNode = new EmployeeNode('Nick');
        $newSubbordinate = new EmployeeNode('Pete');
        $employeeNode->addSubbordinate($newSubbordinate);
        $this->assertSame($employeeNode->getSubbordinates(), [$newSubbordinate]);
    }

    public function testItCanAddTwoSubbordinates()
    {
        $employeeNode = new EmployeeNode('Nick');
        $newSubbordinate1 = new EmployeeNode('Pete');
        $newSubbordinate2 = new EmployeeNode('Barbara');
        $employeeNode->addSubbordinate($newSubbordinate1);
        $employeeNode->addSubbordinate($newSubbordinate2);
        $this->assertSame($employeeNode->getSubbordinates(), [$newSubbordinate1, $newSubbordinate2]);
    }

    public function testItCanFormatEmployeeNode()
    {
        $employeeNode = new EmployeeNode('Nick');
        $formater = \Mockery::mock('Formater, ' . FormatterInterface::class);
        $formater->shouldReceive('format')
            ->with($employeeNode)
            ->andReturn(['a mocked array']);
        $result = $employeeNode->format($formater);
        $this->assertEquals($result, ['a mocked array']);
    }
}
