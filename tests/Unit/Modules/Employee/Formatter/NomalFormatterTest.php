<?php

namespace Tests\Unit\Modules\Employee\Formatter;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Modules\Employee\Formatter\NormalFormatter;
use App\Modules\Employee\TreeBuilder;

class NormalFormatterTest extends TestCase
{
    public function testItCanFormatEmployeeNode()
    {
        $input = collect([
            ['employee_name'=>'Pete','supervisor_name'=>'Nick'],
            ['employee_name'=>'Barbara','supervisor_name'=>'Nick'],
            ['employee_name'=>'Nick','supervisor_name'=>'Sophie'],
            ['employee_name'=>'Sophie','supervisor_name'=>'Jonas']
        ]);
        $tree = app(TreeBuilder::class)->buildTree($input)->format(new NormalFormatter);
        $expected = '{"Jonas":[{"Sophie":[{"Nick":[{"Pete":[]},{"Barbara":[]}]}]}]}';
        $this->assertSame(json_encode($tree), $expected);

    }
}
