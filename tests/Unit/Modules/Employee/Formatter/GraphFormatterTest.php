<?php

namespace Tests\Unit\Modules\Employee\Formatter;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Modules\Employee\TreeBuilder;
use App\Modules\Employee\Formatter\GraphFormatter;

class GraphFormatterTest extends TestCase
{
    public function testItCanFormatEmployeeNode()
    {
        $input = collect([
            ['employee_name'=>'Pete','supervisor_name'=>'Nick'],
            ['employee_name'=>'Barbara','supervisor_name'=>'Nick'],
            ['employee_name'=>'Nick','supervisor_name'=>'Sophie'],
            ['employee_name'=>'Sophie','supervisor_name'=>'Jonas']
        ]);
        $tree = app(TreeBuilder::class)->buildTree($input)->format(new GraphFormatter);
        $expected = '{"text":"Jonas","children":[{"text":"Sophie","children":[{"text":"Nick","children":[{"text":"Pete","children":[]},{"text":"Barbara","children":[]}]}]}]}';
        $this->assertSame($expected, json_encode($tree));
    }
}
