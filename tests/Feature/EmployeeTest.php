<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTest extends TestCase
{
    public function test_it_can_build_correct_tree_format()
    {
        $input = ["Pete" => "Nick", "Barbara" => "Nick", "Nick"=> "Sophie", "Sophie" => "Jonas"];
        $response = $this->json('post', '/api/employees/hierarchies', $input);
        $response->assertOk();
        $this->assertEquals($response->getContent(), '{"Jonas":[{"Sophie":[{"Nick":[{"Pete":[]},{"Barbara":[]}]}]}]}');
    }

    /**
     * @dataProvider invalid_json_input
     */
    public function test_it_has_errors_on_invalid_json($input, $statusCode)
    {
        $response = $this->json('post', '/api/employees/hierarchies', $input);
        $response->assertStatus($statusCode);
    }

    public function invalid_json_input()
    {
        return [
            //empty employee name
            [
                ["Pete" => "Nick", " " => "Nick", "Nick"=> "Sophie", "Sophie" => "Jonas"],
                422
            ],
            //empty supervisor name
            [
                ["Pete" => "Nick", "Barbara" => "Nick", "Nick"=> "  ", "Sophie" => "Jonas"],
                422
            ],
            //empty employee
            [
                [],
                422
            ]
        ];
    }
}
